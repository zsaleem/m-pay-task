## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - React
 - React Hooks
 - React Context API
 - useReducer Hook
 - React Router
 - JSX
 - CSS Grid
 - Styled-Components
 - CSS Animations
 - CSS Media Queries
 - Mobile first responsive
 - ES6+
 - Git
 - Gitflow
 - Github
 - JSDocs
 - NPM/Yarn
 - Sublime Text
 - Font Awesome
 - Mac OS X
 - Google Chrome Incognito
 - Webpack
 - Babel
 - eslint
 - nodejs(16.4.0)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `/root folder/app` directory.
    3. Run `yarn install/npm install` command to download and install all dependencies.
    4. To run this project use `npm run serve` command in command line.
    5. To build the project for production run `npm run build:prod` command. This will build the app for production and put all the files in `/dist` folder.
    6. To run production ready version on local environment, run `npm run serve:prod`. Then go to `http://localhost:8080`.

## Description
Above steps, in getting started section, will install all the dependencies required for this project to run.

For this project I setup the whole development environment from scratch as boilerplates such as `create-react-app` was not allowed to use for this project.

In this task I used `React 17.0.2`. I made use of `React Hooks`, `React Context API`, `useReducer` and `React Router DOM` for routing. I developed the entire project inside `/app` folder. All the components for this project resides in `/app/src/components/` folder. Every component has its own folder with its file name and alongside dependent components folders. `/app/src/components/common` folder has all the components that are common to the the entire application for reusability. I put constants inside `/src/constants/` folder which includes constant variables for `redux actions`. I implemented routing in `/app/App.js` file.

I also created a video from this project which shows the app in action. [Click here](https://www.youtube.com/watch?v=h4TESiI48bI) to watch it in action.

### Development Environments
As I mentioned I developed the whole development environment for this project. In order to achieve that I used `Webpack`. I wrote the `webpack.config.js` file from scratch. What this file basically does that it checks the current `mode` of the running server. If it is `development` then load the development specific web pack file which is `/app/build/development.js` file. If the `mode` is `production` then load production specific file which is `/app/build/production.js`.

For `development` environment, there is no need to build and minify all the resources simply go ahead and execute the project in local environment as is.

For `production` environment, I am adding a tarser plugin to minify all the resources for production release.

The `/app/build/common.js` file is basically contains the common code for both `development` and `production` environment with minor changes just to follow DRY practices.

### Architecture
I had some options regarding choosing the best architecture for this app. One option was to choose simple components base architecture. I did not choose that because it could turn the project into `speghatti code` which is hard to read, maintain and scale.

Another option I had was to choose React Context API alone. With this approach all components had to acces the global state and manipulate is directly from the context which is a bad practice. Which later becomes hard to track data where it is coming from for each component. In addition, it becomes hard to maintain and scale at certain point.

`Redux`, a most widely use state management library for react, was another option. However, I did not choose that because `redux` would have overkill for such a small project. I had to write a lot of boilerplate code.

What I chose was `React Context API with useReducer hook`. With this approach, I could have a global state with up to date data in it. That data is accessible to all the child components. In order to update the global state I had to use `useReducer` hook to access and update the global state. This architecture is also scalable because all I had to do is to keep adding new components and access the data from global state via `useReducer`. If two components required different data from the global state simply add another context and enclose those two components' routes in newly developed context's provider in `App.js` file. Its that easy.

### Performance
In terms of performance, as I mentioned I am using webpack to build the project for production. So it minifies all the resources which reduces it files sizes which loads faster on the network. In addition, I also implemented this project in a certain way that when user it redirecting from `listItems` component to `itemDetail` component then it means we have all the data in the global state so there is no need to make http request to load data. Simply access the data from global state and find that particular item/product by checking in id of the product. This improves performance without making additional http request.

However, if the `itemDetail` component is refreshed or user came to `itemDetail` component via a direct link/url then there will be no data available in global state, in that case it has to make http request to load the details of the item/product.

### Development process
The development process that I followed for this project is `Gitflow` work flow. That means, I have two git branches `master` and `develop`. So I created a `feature branch` from `develop` branch. I develop the whole feature in that branch. Once I am done with it. I `merge` that branch with `develop` branch and push it to remote repo with `develop` branch. When I need to release new feature, I create a new branch `release` with a version number on it. Then I merge that `release` branch into master branch and push it to remote repo's `master` branch.

### Code Quality
I wrote the entire project in components architecture which means everything is a component. I wrote all the code very readable and easy follow. I also used `JSDocs` to document the entire code. In addition, I used `eslint` to keep the quality of the code intact.

## Notes
The design I was provided with was mobile friendly design so I had no design for the desktop version of this project. Still made this project `responsive` which means it works on both mobile phones and desktops.

According to specs, the search field was not in the specs and requirements to be implemented so I simply put it there in the design with no functionality because of no specs privided therefore, I decided this would be out of the scope of this task.

I added more `description` to the first item in `/api-server/json/all.json` file just to test the long description with multiple paragraphs on the user interface.

According to the design, it was not clear whether I need that `Sold` banner on the item in `itemDetail` component. Since I thought this is a mandatory information users need to know so I put it there on `itemDetail` component.

If you have any question or having problem running this app then please let me know and I'll answer your questions help you out with running this project.
