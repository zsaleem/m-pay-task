import * as types from '../../constants/';

/** 
 * Initial State
 * 
 * @type {State} 
 */
export const initialState = {
	loading: false,
	error: false,
};

/**
 * Items Reducer.
 *
 * @type {Reducer} 
 * @param {Store} state - The current state
 * @param {Action} action - An action
 * @return {Store} The updated state
 */
export const ItemsReducer = (state = initialState, action) => {
	switch(action.type) {
		case types.FETCH_ITEMS_LIST_REQUESTED:
			return {
				loading: true,
				error: false,
				items: [],
			}
		case types.FETCH_ITEMS_LIST_SUCCESS:
			return {
				loading: false,
				error: false,
				items: action.payload,
			}
		case types.FETCH_ITEMS_LIST_FAIL:
			return {
				loading: false,
				error: true,
			}
		default:
			return state;
	}
}
