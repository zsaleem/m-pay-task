import {
  createContext,
  useEffect,
  useContext,
  useState,
  useReducer,
} from 'react';
import { BASE_URL } from '../configs/';
import {
  fetchItemsRequested,
  fetchItemsSuccess,
  fetchItemsFail,
} from './actions/itemsActions';
import * as types from '../constants/';

// Create Items Context
export const ItemsContext = createContext({});

// export useItemsContext to use as Context.
export const useItemsContext = () => useContext(ItemsContext);

/**
* Item component that renders single item/product on the screen,
* 
* @param {object} children - elements.
* @param {object} initialState - Initial state.
* @param {function} reducer - Reducer.
* 
* @returns {Provider} Provider.
*/
const ItemsProvider = ({ children, initialState, reducer }) => {
  /**
   * Declare globalState state variable. This is an object that
   * contains properties like loading, error and items. Second, is
   * the dispatch function to dispatch actions.
   * 
   * @type {boolean, function}
   */
  const [globalState, dispatch] = useReducer(reducer, initialState);

  /**
  * fetchItems function to fetch data/items/products from server.
  */
  const fetchItems = async () => {
    // dispatch request action to indicate that fetch items has been
    // requested.
    dispatch(fetchItemsRequested());
    try {
      // call fetch function with url to get items
      const response = await fetch(`${BASE_URL}/items`);
      // check if there is any error in network
      if (!response.ok) {
        // dispatch fail action upon error in network.
        dispatch(fetchItemsFail(response.status));
        return;
      }
      // convert response to json.
      const items = await response.json();
      // dispatch success action as the items are received successfully.
      dispatch(fetchItemsSuccess(items.data));
    } catch (err) {
      // dispatch fail action because there is some error in network.
      dispatch(fetchItemsFail(err));
    }
  };

  return (
    <ItemsContext.Provider
      value={{
        fetchItems,
        globalState,
      }}
    >
      {children}
    </ItemsContext.Provider>
  );
};

export default ItemsProvider;
