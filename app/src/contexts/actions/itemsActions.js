import * as types from '../../constants/';

/**
 * Triggers List Items HTTP request to fetch data for all items/products 
 * for listItems component.
 * 
 * @type {ActionCreator}
 * @return {Object} A new object that contains the type of action.
 */
export const fetchItemsRequested = () => {
  return {
    type: types.FETCH_ITEMS_LIST_REQUESTED,
  }
};

/**
 * Gets called when API request is successful.
 * 
 * @type {ActionCreator}
 * @return {Object} A new object that contains the type of action and payload/data.
 */
export const fetchItemsSuccess = (payload) => {
  return {
    type: types.FETCH_ITEMS_LIST_SUCCESS,
    payload,
  }
};

/**
 * Gets called when API request is failed.
 * 
 * @type {ActionCreator}
 * @return {Object} A new object that contains the type of action and error.
 */
export const fetchItemsFail = (error) => {
  return {
    type: types.FETCH_ITEMS_LIST_FAIL,
    error,
  }
};
