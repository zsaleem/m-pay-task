import styled from 'styled-components'

// This is span html element which shows if the item
// sold out. I am setting its position absolute, and placing
// it at the top left corner in such a way that half of this
// element is shown and half of it is hidden due to ItemContainer
// is set with 'overflow: hidden'. Its text color is white with
// certain width and height and background color red. I am rotating
// it with 317 degree using the transform property to give it a
// a certain angle so that it matches with the design specs provided
// with this coding task. Finally, I am adding some padding to set
// the 'SOLD' text at the bottom and font weight bold.
const Wrapper = styled.span`
	position: absolute;
  top: ${({ top }) => top};
  left: ${({ left }) => left};
  color: #fff;
  width: 108px;
  height: 100px;
  background-color: #ff0000;
  transform: rotate(317deg);
  padding: 77px 30px 0;
  font-weight: bold;
`;

/**
* Sold component that renders the sold badge on top of item if it is sold out.
* 
* @param {object} position - It contains top and left position of this component.
* @param {number} top - It is the top position of the Sold component.
* @param {number} left - It is the left position of the Sold component.
* 
* @returns {JSX} Component User Interface.
*/
const Sold = ({ position: { top, left }}) => {
	return (
	<Wrapper top={top} left={left}>SOLD</Wrapper>
)};

export default Sold;