import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faHeart as farHeart,
	faCommentAlt,
	faFlag as fasFlag,
} from '@fortawesome/free-regular-svg-icons';

// Main container for Like component. I am setting its position to
// absolute and pacing it to the right side of its parent container
// with light grey color of the text. I am giving some margin to the
// heart icon and changing its font size with transparent fill color.
const ButtonContainer = styled.span`
	position: absolute;
  right: 10px;
  color: #8c8c8c;

  .fa-heart {
  	margin-right: 2px;
  	font-size: 15px;
		-webkit-text-fill-color: transparent;
	}
`;

const Wrapper = styled.span`
	padding: 10px;
	margin-right: 2px;
	border-radius: 200px;
	background-color: #e9e9e9;
`;

const Label = styled.span`
	margin-left: 3px;
`;

/**
* Button component that renders buttons with icons,
* 
* @param {number} likes - It is the total number of likes.
* @param {string} label - It is the label of the button.
* @param {string} type - It is the type of the button.
* 
* @returns {JSX} Component User Interface.
*/
const Button = ({ likes, label, type }) => (
	<ButtonContainer className={type ? type : 'report'}>
		{
			type === 'likes'
			?
				label
				?
				<Wrapper>
					<FontAwesomeIcon icon={farHeart} />
					<Label>{label}</Label>
				</Wrapper>
				:
				<FontAwesomeIcon icon={farHeart} />
			:
			type === 'comments'
			?
			<Wrapper>
				<FontAwesomeIcon icon={faCommentAlt} />
				<Label>{label}</Label>
			</Wrapper>
			:
			<Wrapper>
				<FontAwesomeIcon icon={fasFlag} />
			</Wrapper>
		}
		{likes}
	</ButtonContainer>
);

export default Button;
