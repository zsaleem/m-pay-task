import { Link } from 'react-router-dom';
import styled from 'styled-components';

// Main wrapper nav html element. I am setting it fixed to
// to the top left corner of the view port with 100% width
// 50pc height. I am setting some padding, background color
// and z index to this element.
const Wrapper = styled.nav`
	position: fixed;
	top: 55px;
	left: 0;
	width: 100%;
	height: 50px;
	padding: 18px;
	background-color: #ffffff;
	z-index: 1;
`;

// Link/a element. I am setting them relative with inline block
// no decoration with some space on the right with color greyish
// and red in color upon active.
const NavLink = styled(Link)`
	position: relative;
	display: inline-block;
	text-decoration: none;
	margin-right: 18px;
	color: #8c8c8c;
	&.active {
		color: red;
	}
`;

// This is a span element which is a small arrow below active 
// link navigation.
const Active = styled.span`
	position: absolute;
	left: 15px;
  bottom: -10px;
	width: 0; 
  height: 0; 
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-bottom: 5px solid #efefef;
`;

/**
* Navigation component that renders the top navigation on listItem component.
* 
* @returns {JSX} Component User Interface.
*/
const Navigation = () => (
	<Wrapper>
		<NavLink to='#'>スマホ</NavLink>
		<NavLink to='#'>エンタメ</NavLink>
		<NavLink to='#' className='active'>
			メンズ
			<Active></Active>
		</NavLink>
		<NavLink to='#'>すべて</NavLink>
		<NavLink to='#'>レデイース</NavLink>
	</Wrapper>
);

export default Navigation;
