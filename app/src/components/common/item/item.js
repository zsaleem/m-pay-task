import { useEffect, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Name from '../name/name';
import Price from '../price/price';
import Button from '../button/button';
import Sold from '../sold/sold';

// It is the main container of the Item component.
// It is a Link. I am setting its overflow to hidden
// to add mask for `SOLD` label. I am setting text
// decoration to none with white background and some
// animation.
const ItemContainer = styled(Link)`
	overflow: hidden;
	text-decoration: none;
	background-color: #ffffff;
	animation: expand .3s ease-in-out;
	@keyframes expand {
	  from {
	    transform: scale(0);
	  }
	}
`;

// It is the 'figure' html element which holds the image
// and the rest of the details. It setting it as relative
// zero margin and padding.
const ImageContainer = styled.figure`
	position: relative;
	margin: 0;
	padding: 0;
`;

// This is the 'img' html element with 100% width and with
// modifier class show to display it when all the images are
// loaded from API call. It is useful to put a local placeholder
// image and wait for the images to look from the network, when
// they are all loaded successfully then replace the placeholder
// with image from the network.
const Image = styled.img`
	width: 100%;
	&.show {
		display: block;
	}
`;

// It is a figcaption html element with some padding.
const Caption = styled.figcaption`
	padding: 10px;
`;

/**
* Item component that renders single item/product on the screen,
* 
* @param {number} category_id - It is the id of the current item.
* @param {string} image - It is a string URL to the image on the network.
* @param {boolean} is_sold_out - It sets the current product/item as either
* beign sold out or still in the stock.
* @param {string} name - It is the name of the item.
* @param {number} price - It is the price of the item.
* @param {number} like_count - It is the total number of count received by
* item from users.
* 
* @returns {JSX} Component User Interface.
*/
const Item = ({
	item: {
		id,
		image,
		is_sold_out,
		name,
		price,
		like_count,
	},
}) => {
	/**
	 * Declare imgLoaded state variable. This is a boolean variable that
	 * indicates that the image of the item is loaded from the network or
	 * not. If it is loaded the imgLoaded is true otherwise false.
	 * 
	 * @type {boolean, function}
	 */
	const [imgLoaded, setImgLoaded] = useState(false);
	/**
	 * Reference to imgEl img tag DOM element.
	 * 
	 * @type {dom}
	 */
	const imgEl = useRef(null);
	const soldComponentPosition = { top: '-50px', left: '-48px', };
	/**
	* When the image is loaded then call this function and set imgLoaded
	* state to true.
	* 
	* @returns {function} setImgLoaded.
	*/

	const onImageLoaded = () => setImgLoaded(true);
	/**
	 * This runs when imgEl is changed or updated.
	 *
	 * @type {function}
	 */
  useEffect(() => {
  	// Reference to img element
    const imgElCurrent = imgEl.current;
    // check if img element exist.
    if (imgElCurrent) {
    	// add 'load' event listener to img tag and call onImageLoaded
    	// when it is successfully loaded.
      imgElCurrent.addEventListener('load', onImageLoaded);
      // finally remove the load event listener from img element.
      return () => imgElCurrent.removeEventListener('load', onImageLoaded);
    }
  }, [imgEl]);

	return (
		<>
			{/* Main Item Container that holds all the element of this item */}
			<ItemContainer to={`/detail/${id}`}>
				{/* Image container which is basically figure html tag */}
				<ImageContainer>
				 {/*
				 		An img html tag, I am setting ref, src and alt attributes with class
				 */}
					<Image
						ref={imgEl}
						src={`${image}`}
						alt={image}
						className={ imgLoaded ? 'show' : '' }
					/>
					{/*
						figcaption html tag which holds some information regarding this item
						such as SOLD item, name, price and number of likes.
					*/}
					<Caption>
						{
							is_sold_out
							?
							<Sold position={soldComponentPosition} />
							:
							''
						}
						<Name name={name} size='18px' fontColor='919191' />
						<Price price={price} fontSize='18px' />
						<Button likes={like_count} type='likes' />
					</Caption>
				</ImageContainer>
			</ItemContainer>
		</>
	);
}

export default Item;
