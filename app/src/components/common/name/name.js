import styled from 'styled-components';

// Main container of the Name component which is a div html
// element. I am setting its color greyish with text overflow
// and font size.
const NameContainer = styled.div`
	color: ${({fontColor}) => `#${fontColor}`}; // #919191
	text-overflow: ellipsis;
	font-size: ${props => props.fontSize};
`;

/**
* Name component that renders the name of the item,
* 
* @param {string} name - It is the name of the item.
* @param {number} size - It is the font size of this component.
* @param {string} fontColor - It is the color of the font size.
* 
* @returns {JSX} Component User Interface.
*/
const Name = ({ name, size, fontColor }) => (
	<NameContainer className='name' fontSize={size} fontColor={fontColor}>
		{
			/* I am checking if the name is too long */
			name?.length > 15
			?
			/* Then remove the additional string from the name and append ... */
			name.substring(0, 21) + '...'
			:
			/* else display name as is because it is short */
			name
		}
	</NameContainer>
)

export default Name;
