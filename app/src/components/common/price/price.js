import styled from 'styled-components';

// Main span container. I am setting its color, bold text
// and font size which is dynamic.
const PriceContainer = styled.span`
	color: #545454;
	font-weight: bold;
	font-size: ${({ fontSize }) => fontSize};
`;

/**
* Price component that shows the price of current item.
* 
* @param {number} price - It is the price of this item.
* @param {number} fontSize - It is the font size of this item.
* 
* @returns {JSX} Component User Interface.
*/
const Price = ({ price, fontSize }) => (
	<PriceContainer className='price' fontSize={fontSize}>¥{price}</PriceContainer>
);

export default Price;
