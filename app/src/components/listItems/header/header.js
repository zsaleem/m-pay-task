import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faBell as farBell,
} from '@fortawesome/free-regular-svg-icons';
import {
	faCheck,
} from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

// Main wrapper div with global styles for this component.
const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 55px;
  background-color: #ffffff;
  border-bottom: 1px solid #c8c7cb;
  z-index: 1;
`;

// Grid div element to convert Header into grid with 3
// columns.
const Grid = styled.div`
	padding: 18px;
	display: grid;
	grid-template-columns: .5fr 2.5fr 1fr;
`;

// Hamburger menu with a certain width.
const Hamburger = styled.div`
	width: 30px;
`;

// Individual lines within Hamburger menu.
const Line = styled.span`
	display: block;
	margin-bottom: 5px;
	border: 1px solid #c8c7cb;
`;

// Input field with a placeholder in it.
// displayed in second column of header.
const SearchField = styled.input`
	width: 100%;
	padding: 10px;
	margin-top: -8px;
	border-radius: 10px;
	border: 0 solid #eceaeb;
	background-color: #eceaeb;
`;

// Bell icon displayed in third column of the grid
const BellIcon = styled(Link)`
	margin-left: 20px;
	color: #c8c7cb;
`;

// Check icon displayed in third column of the grid. 
const CheckIcon = styled(Link)`
	margin-left: 20px;
	color: #c8c7cb;
`;

const Header = () => {
	return (
		<Wrapper>
			<Grid>
				<Hamburger>
					<Line />
					<Line />
					<Line />
				</Hamburger>
				<SearchField type='text' name='search' placeholder='検 索'>
				</SearchField>
				<div>
					<BellIcon to=''>
						<FontAwesomeIcon icon={farBell} />
					</BellIcon>
					<CheckIcon to=''>
						<FontAwesomeIcon icon={faCheck} />
					</CheckIcon>
				</div>
			</Grid>
		</Wrapper>
	);
}

export default Header;
