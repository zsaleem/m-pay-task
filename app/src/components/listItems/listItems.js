import { useEffect, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Item from '../common/item/item';
import Preloader from '../common/preloader/preloader';
import Navigation from '../common/navigation/navigation';
import Header from './header/header';
import { useItemsContext } from '../../contexts/itemsContext';

export const Container = styled.div`
	max-width: 1440px;
	margin: auto;
	margin-top: 105px;
	padding: 10px;
	display: grid;
	grid-gap: 10px;
	grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
	@media (max-width: 768px) {
		grid-template-columns: 1fr 1fr 1fr 1fr;
	}
	@media (max-width: 450px) {
		grid-template-columns: 1fr 1fr;
	}
`;

/**
* ListItems component that renders all items in a grid layout.
* 
* @returns {JSX} Component User Interface.
*/
const ListItems = () => {
	/**
	 * Extract fetchItems, loading, error and items properties from global state.
	 * @type {object}
	 * @property {function} fetchItems - function to get items from remote server.
	 * @property {object} globalState - global state of this application contains
	 * global items and properties.
	 * @property {Boolean} loading - true/false
	 * @property {Boolean} error - true/false
	 * @property {Array} items - List of all items/products.
	 */
	const {
		fetchItems,
		globalState: {
			loading,
			error,
			items,
		}
	} = useItemsContext();
	/**
	 * This runs when this component is rendered first time.
	 *
	 * @type {function}
	 */
	useEffect(() => {
		// set the title of this page.
		document.title = 'List Items Page';
		// check if items undefined, if it is undefined then there
		// are no items in global state to render in this component
		if (items !== undefined) {
			return;
		}
		// If item is not defined then check if there are items in it.
		if (items?.length > 0) {
			return;
		}
		// Otherwise there are no items in items array so call fetchItems
		// function in the itemsContext to fetch items/products.
		fetchItems();
	}, []);

	return (
		<>
			<Header />
			{/* Navigation component */}
			<Navigation />
			{/* Main div element with display: grid */}
			<Container>
				{
					/* check if network request is in progress */
					loading
					?
					/* show a preloader */
					<Preloader />
					:
					/* iterate over items to display individual items/products */
					items?.map(item => (
						<Item item={item}  key={Math.random()} />
					))
				}
			</Container>
		</>
	);
}

export default ListItems;
