import Button from '../../common/button/button';
import styled from 'styled-components';

// Main wrapper div element with common styles for
// both desktop and mobile component.
const Wrapper = styled.div`
	height: 30px;
	position: relative;
	.likes {
		left: 20px;	
	}
	.comments {
		left: 160px;
	}
	.report {
		right: 10px;
	}
`;

// Desktop specific styles for this component.
const DesktopWrapper = styled.div`
	display: block;
  margin-top: 75px;
  @media (max-width: 600px) {
		display: none;
	}
`;

// Mobile specific styles for this component.
const MobileWrapper = styled.div`
	display: none;
	@media (max-width: 600px) {
		display: block;
	}
`;

/**
* DesktopItemActions component that renders action icons for currentItem.
* 
* @param {number} likeCount - it holds the total number of like 
* for currenItem.
* 
* @returns {JSX} Component User Interface.
*/
export const DesktopItemActions = ({ likeCount }) => {
	return (
		<Wrapper>
			<DesktopWrapper>
				<Button label='いいね!' type='likes' likes={likeCount} />
				<Button label='コメント' type='comments' />
				<Button type='report' />
			</DesktopWrapper>
		</Wrapper>
	);
};

/**
* MobileItemActions component that renders action icons for currentItem.
* 
* @param {number} likeCount - it holds the total number of like 
* for currenItem.
* 
* @returns {JSX} Component User Interface.
*/
export const MobileItemActions = ({ likeCount }) => {
	return (
		<Wrapper>
			<MobileWrapper>
				<Button label='いいね!' type='likes' likes={likeCount} />
				<Button label='コメント' type='comments' />
				<Button type='report' />
			</MobileWrapper>
		</Wrapper>
	);
};
