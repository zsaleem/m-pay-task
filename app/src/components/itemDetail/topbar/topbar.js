import { Link } from 'react-router-dom';
import Name from '../../common/name/name';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

// Main wrapper div element which is fixed at the top
// left corner of the viewport with 100% width and height
// 50px. It has some padding, background color, border and
// z index.
const TopBarWrapper = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 50px;
	padding: 18px;
	background-color: #ffffff;
	border-bottom: 2px solid #dddddf;
	z-index: 1;

	div {
		margin-left: 25px;
	}
`;

// The back button which is displayed block and 
// positioned left with some space on the right
// and color greyish. No line height and decoration
// and setting some transform property.
const Back = styled(Link)`
	display: block;
	position: absolute;
  top: 28px;
	margin-right: 20px;
	color: #dddddf;
  text-decoration: none;
  line-height: 0px;
  transform: scaleY(2.0);
`;

// The search icon
const Search = styled(Link)`
	position: absolute;
  right: 70px;
  top: 22px;
	color: #dddddf;
`;

// The ios like share icon
const Share = styled(Link)`
	position: absolute;
  right: 30px;
  top: 22px;
	color: #dddddf;

	svg {
		bottom: 4px;
    position: relative;
	}

	span {
		display: inline-block;
		position: absolute;
		right: -2px;
    top: 6px;
    height: 10px;
    width: 18px;
    padding: 0 1px 0px;
    border-bottom: 1px solid #dddddf;
		border-right: 1px solid #dddddf;
		border-left: 1px solid #dddddf;
	}
`;

/**
* Topbar component which renders the topbar for the detail 
* component.
* 
* @param {string} name - The name of the currentItem
* 
* @returns {JSX} Component User Interface.
*/
const Topbar = ({ name }) => {
	return (
		<TopBarWrapper>
			<Back to='/'>
				{'<'}
			</Back>
			<Name name={name} size='22px' fontColor='202020' />
			<Search to='#'>
				<FontAwesomeIcon icon={faSearch} />
			</Search>
			<Share to='#'>
				<FontAwesomeIcon icon={faArrowUp} />
				<span></span>
			</Share>
		</TopBarWrapper>
	);
}

export default Topbar;
