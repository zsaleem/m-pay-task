import { Link } from 'react-router-dom';
import Price from '../../common/price/price';
import styled from 'styled-components';
// Main wrapper div element for shipping component.
// I set it up relative with some top margin, width
// and padding properties with white font color and
// dark background color. For small screen sizes, 
// I make its position fixed and place it at the bottom
// of the viewport.
const Wrapper = styled.div`
	position: relative;
  margin-top: 20px;
  width: 100%;
  padding: 18px 5px;
  color: #ffffff;
  background-color: #39211e;

  .price {
  	color: #ffffff;
  }

  @media (max-width: 600px) {
		position: fixed;
		bottom: -1px;
	  left: 0;
	}
`;

// The red button is set as absolute and on the right
// side of the bar with white font color and red
// background color.
const CartButton = styled(Link)`
	position: absolute;
  right: 7px;
  top: 3px;
  padding: 15px;
	text-decoration: none;
  color: #ffffff;
  background-color: #d80024;
`;

// shipping fee text with small font size and left margin.
const Shippingfee = styled.span`
	font-size: 10px;
  margin-left: 5px;
`;

/**
* Shipping component that renders shipping details for currentItem.
* 
* @param {object} item - it holds item's price and shipping fee.
* @param {number} price - it is the price of the currentItem.
* @param {string} shipping_fee - it is the shipping fee of the currentItem.
* 
* @returns {JSX} Component User Interface.
*/
const Shipping = ({ item: { price, shipping_fee } }) => {
	return (
		<Wrapper>
			<Price price={price} />
			<Shippingfee>{shipping_fee}</Shippingfee>
			<CartButton to='#'>購入手続きへ</CartButton>
		</Wrapper>
	);
}

export default Shipping;
