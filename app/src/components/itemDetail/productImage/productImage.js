import Name from '../../common/name/name';
import Sold from '../../common/sold/sold';
import { MobileItemActions } from '../itemActions/itemActions';
import styled from 'styled-components';

import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';

// Main image container div element with relative position
// no margin and padding with overflow hidden. And moving
// it down a little with margin top.
const ImageContainer = styled.figure`
	position: relative;
	margin: 0;
	padding: 0;
	overflow: hidden;
	margin-top: 50px;
`;

// Image of the product with 100% width
const Image = styled.img`
	width: 100%;
`;

// figcaption with styles for item's names.
const Caption = styled.figcaption`
	.name {
		padding: 0 20px;
		margin-bottom: 30px;
	}
`;

// I am setting the color for description section.
const Description = styled.div`
	color: #919191;
`;

// The name/title of the item with some padding, bold
// text and font size 16px background color greyish.
const Title = styled.h3`
	padding: 18px 18px 5px 18px;
	font-weight: normal;
	font-size: 16px;
	background-color: #efefef;
`;

// p html element with some padding and margin bottom.
const Desc = styled.p`
	padding: 0 18px;
	margin-bottom: 55px;
`;

/**
* ProductImage component that renders product image with
* details for currentItem.
* 
* @param {object} item - it holds item's details.
* @param {string} image - it is the url of the image.
* @param {string} name - name of the currentItem.
* @param {number} like_count - total number of likes.
* @param {string} description - description of the item.
* @param {boolean} is_sold_out - shows if the item is sold out or not.
* 
* @returns {JSX} Component User Interface.
*/
const ProductImage = ({
	item: {
		image,
		name,
		like_count,
		description,
		is_sold_out,
	}
}) => {
	// setting some properties to position Sold component
	const soldComponentPosition = { top: '-50px', left: '-50px', };

	return (
		<ImageContainer>
			<Carousel showThumbs={false} showStatus={false}>
				{
					[image].map(imagePath => (
						<div key={Math.random()}>
		          <img src={imagePath} />
		        </div>
					))
				}
      </Carousel>
			<Caption>
				{
					is_sold_out
					?
					<Sold position={soldComponentPosition} />
					:
					''
				}
				<Name name={name} size='22px' fontColor='373738' />
				<MobileItemActions likeCount={like_count} />
				<Description>
					<Title>商品の説明</Title>
					{description?.split('\n').map(des => (
						<Desc key={Math.random()}>{des}</Desc>
					))}
				</Description>
			</Caption>
		</ImageContainer>
	);
}

export default ProductImage;
