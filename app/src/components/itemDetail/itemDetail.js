import { useState, useEffect } from 'react';
import { Link, useLocation, Redirect } from 'react-router-dom';
import Topbar from './topbar/topbar';
import ProductImage from './productImage/productImage';
import Shipping from './shipping/shipping';
import Preloader from '../common/preloader/preloader';
import { DesktopItemActions } from './itemActions/itemActions';
import { BASE_URL } from '../../configs/';
import { useItemsContext } from '../../contexts/itemsContext';
import styled from 'styled-components';

// Main wapper div element with max width, centered and min
// height and background color styles.
const Wrapper = styled.div`
	max-width: 1400px;
	margin: auto;
	min-height: 100vh;
	background-color: #ffffff;
	.message {
		margin-top: 49px;
	}
`;

// The div element with grid display. For desktops there
// is one layout and for mobile there is specific layout
// mentioned in design specs for this coding task.
const Grid = styled.div`
	display: grid;
	grid-template-columns: 2fr 1fr;
	grid-gap: 10px;
	animation: expand .3s ease-in-out;
	@keyframes expand {
	  from {
	    transform: scale(0);
	  }
	}
	@media (max-width: 900px) {
		grid-template-columns: 2fr 2fr;
	}
	@media (max-width: 600px) {
		grid-template-columns: 1fr;
	}
`;

/**
* ListDetail component that renders one item/product with details.
* 
* @returns {JSX} Component User Interface.
*/
const ItemDetail = () => {
	/**
	 * Extract fetchItems, loading, error and items properties from global state.
	 * @type {object}
	 * @property {function} fetchItems - function to get items from remote server.
	 * @property {object} globalState - global state of this application contains
	 * global items and properties.
	 * @property {Boolean} loading - true/false
	 * @property {Boolean} error - true/false
	 * @property {Array} items - List of all items/products.
	 */
	const {
		fetchItems,
		globalState: {
			loading,
			error,
			items,
		}
	} = useItemsContext();
	/**
	 * Declare 'localLoading' state variable. This shows and hides preloader.
	 * 
	 * @type {boolean, function}
	 */
	const [localLoading, setLocalLoading] = useState(false);
	/**
	 * Declare 'currentItem' state variable. This holds the item for this component.
	 * 
	 * @type {object, function}
	 */
	const [currentItem, setCurrentItem] = useState({});
	/**
	 * Get 'pathname' from browser url.
	 * 
	 * @type {object}
	 */
	const { pathname } = useLocation();
	/**
	 * Extract id number from 'pathname'
	 * 
	 * @type {string}
	 */
	const itemId = pathname.split('/')[2];

	/**
	 * This runs on first render of ItemDetail component and when
	 * itemId is updated.
	 * 
	 * @type {function}
	 */
	useEffect(() => {
		// check if user entered an id that is not a number.
		if (isNaN(itemId)) {
			setCurrentItem({ message: 'There is no item with that id.' })
			return;
		}
		// Checks if there are items in global state. If there
		// are then get the currentItem from global state instead
		// of making http request to get it from remote server. 
		// This enhances performance for the app.
		if (items !== undefined && items.length > 0) {
			// iterate through items array to find the currentItem
			items.find(item => {
				// checks the id property of item from items with itemId
				if (item.id === itemId) {
					// set the found item to currentItem
					setCurrentItem(item);
				}
				// check if all the items are iterated and item not found.
				if (itemId > items.length - 1) {
					setCurrentItem({ message: 'Item not found.' });
				}
			});
		} else { // If there are no items in global state then make http request.
			// set the localLoading to true to show preloader.
			setLocalLoading(true);
			/**
		 	 * fetchItems function to load currentItem from remote server.
		 	 * 
			 * @type {function}
			 */
			const fetchItem = async () => {
				// call remote server with API end point.
				const response = await fetch(`${BASE_URL}/items/${itemId}`);
				// check if the remote request is failed.
				if (!response.ok) {
					// throw error if it is failed.
					throw new Error(`Something went wrong, status: ${response.status}`);
				}
				// convert the response into json.
				const item = await response.json();
				// check if item is not found in remote request.
				if (item == null) {
					setCurrentItem({ message: 'Item not found.' });
				} else {
					// set currentItem to loaded item.
					setCurrentItem(item);
				}
				// set localLoading to false to hide preloader.
				setLocalLoading(false);
			}
			// call fetchItem function to execute.
			fetchItem();
		}
	}, [itemId]);

	/**
	 * This runs on first render of ItemDetail component and when
	 * currentItem is changed/updated.
	 * 
	 * @type {function}
	 */
	useEffect(() => {
		// set the title of this page with the name of the currentItem.
		document.title = currentItem?.name || 'Detail Page';
	}, [currentItem]);

	return (
		<>
			{/* Main wrapper */}
			<Wrapper>
				{/* Topbar which contains the name of item and icons */}
				<Topbar name={currentItem?.name} />
				{
					/* global context loading property to show preloader */
					loading
					?
					/* Preloader component */
					<Preloader />
					:
						/* localLoading state variable for showing preloader */
						localLoading
						?
						/* Preloader component */
						<Preloader />
						:
							currentItem.hasOwnProperty('message')
							?
							<p className='message'>{currentItem.message}</p>
							:
							/* Grid component to show currentItem in 2 columns grid for desktop */
							<Grid>
								{/* ProductImage component */}
								<ProductImage item={currentItem} />
								<div>
									{/* Desktop specific actions */}
									<DesktopItemActions likeCount={currentItem?.like_count} />
									{/* Shipping component fixed at the bottom of the screen */}
									<Shipping item={currentItem} />
								</div>
							</Grid>
				}
			</Wrapper>
		</>
	);
}

export default ItemDetail;
