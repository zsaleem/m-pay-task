import {
  BrowserRouter,
  Route,
  Switch,
  HashRouter,
} from 'react-router-dom';
import NotFound from './components/notFound/notFound'
import ListItems from './components/listItems/listItems';
import ItemDetail from './components/itemDetail/itemDetail';

/**
* In App component I am setting up all the routes for this entire
* application.
* 
* @returns {JSX} Routes.
*/
const App = () => {
	return (
		<HashRouter>
      <div>
        <Switch>
          <Route path='/' exact><ListItems /></Route>
          <Route path='/list' exact><ListItems /></Route>
          <Route path='/detail/:id' exact><ItemDetail /></Route>
          <Route path='/notfound'><NotFound /></Route>
          <Route path='*'><NotFound /></Route>
        </Switch>
      </div>
    </HashRouter>
	);
}

export default App;
