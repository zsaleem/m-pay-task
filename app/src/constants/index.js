// Constants variables for dispatching events used for reducer to catch/listen.
export const FETCH_ITEMS_LIST_REQUESTED = 'FETCH_ITEMS_LIST_REQUESTED';
export const FETCH_ITEMS_LIST_SUCCESS = 'FETCH_ITEMS_LIST_SUCCESS';
export const FETCH_ITEMS_LIST_FAIL = 'FETCH_ITEMS_LIST_FAIL';
