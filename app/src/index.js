import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import ItemsProvider from './contexts/itemsContext';
import { initialState, ItemsReducer } from './contexts/reducer/itemsReducer';

import './index.css';
// I am providing ItemsProvider to App component so the ItemsContext is
// available globally in all components.
ReactDOM.render(
  <React.StrictMode>
    <ItemsProvider initialState={initialState} reducer={ItemsReducer}>
      <App />
    </ItemsProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
